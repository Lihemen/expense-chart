# Frontend Mentor - Expenses chart component solution

This is a solution to the [Expenses chart component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/expenses-chart-component-e7yJBUdjwt). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the bar chart and hover over the individual bars to see the correct amounts for each day
- See the current day’s bar highlighted in a different colour to the other bars
- View the optimal layout for the content depending on their device’s screen size
- See hover states for all interactive elements on the page

### Links

- Solution URL: [Repository](https://gitlab.com/Lihemen/expense-char)
- Live Site URL: [Expense Chart](https://expense-chart-lih.netlify.app/)

## My process

### Built with

- Semantic HTML5 markup
- SCSS
- CSS custom properties
- Flexbox
- Desktop-first workflow

## Author

- Website - [Hemense Lan](https://hemense.net)
- Frontend Mentor - [@lihemen](https://www.frontendmentor.io/profile/lihemen)
- Twitter - [@li_hemen](https://www.twitter.com/li_hemen)
