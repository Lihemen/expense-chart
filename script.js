const bar_markup = `
  <div class="bar__group">
    <span class="bar__value">$24.04</span>
    <div class="bar"></div>
    <span class="bar__label">wed</span>
  </div>`;

const chart_container = document.querySelector(".chart__container");

function populateChart() {
  const xhr = new XMLHttpRequest();

  // GET Data from data.json
  xhr.open("GET", "data.json", true);
  xhr.send(null);

  // Onload
  xhr.onload = function () {
    if (this.status === 200) {
      const data = JSON.parse(this.responseText);
      data.forEach((chart) => {
        chart_container.innerHTML += `
          <div class="bar__group">
            <span class="bar__value">$${chart.amount}</span>
            <div class="bar"  style="height: ${chart.amount * 2}px;"></div>
            <span class="bar__label">${chart.day}</span>
          </div>`;

        highlightCurrentBar();
      });
    }
  };
}

populateChart();

function highlightCurrentBar() {
  const bars = document.querySelectorAll(".bar");

  const currentDay = new Date()
    .toLocaleDateString("default", {
      weekday: "short",
    })
    .toLowerCase();

  bars.forEach((bar) => {
    const barLabel = bar.parentElement.querySelector(".bar__label");

    if (barLabel.innerHTML === currentDay) {
      bar.classList.add("current");
    }
  });
}
